<?php
  require_once('../private/initialize.php');

  session_start();
  $products = $_SESSION['products'];
  $sku = [];
  for ($i=0; $i < count($products); $i++) {
    $sku[] = $products[$i]->get_sku();
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Add Product</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo url_for('/stylesheets/styles.css'); ?>" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo url_for('/scripts/createAttribute.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/scripts/fieldValidation.js'); ?>"></script>
  </head>
  <body>

    <h2>Add New Product</h2><hr>
    <div id="holder">
      <form id="mainform" method="post" onsubmit="return isValid()" action="<?php echo url_for('/create.php'); ?>" >
        <label class="flables" for="sku">SKU</label>
        <input id="skuinput" class="addinput" type="text" name="sku" onchange="return validation()" placeholder="Type product's SKU here" required></br>

        <label class="flables" for="name">Name</label>
        <input class="addinput" type="text" name="name" placeholder="Type product's name here" required></br>

        <label class="flables" for="price">Price</label>
        <input class="addinput" type="number" name="price" placeholder="Type product's price here" required></br>

        <label class="flables" for="type">Type</label>
        <select id="typeswitcher" name="type" placeholder="">
          <option value="size">DVD-disc</option>
          <option value="weight">Book</option>
          <option value="dimensions">Furniture</option>
        </select>
        <p>Please provide additional information about the product:</p>
        <div id="attribute"></div>

        <input class="addinput" type="submit" value="Add Product"/>
        <div id="txtHint"></div>
      </form>
      <a id="backtomenubtn" href=<?php echo url_for('/index.php'); ?>>Cancel and Return</a>
    </div>

  </body>

</html>
