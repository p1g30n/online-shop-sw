<?php

  require_once('../private/initialize.php');

  session_start();

  $products = $_SESSION['products'];

  for ($i=0; $i < count($products); $i++) {
    if (!empty($_POST[$products[$i]->get_id()]) && $_POST[$products[$i]->get_id()] == 'on') {
      delete_products_from_db($products[$i]->get_id());
    }
  }

  redirect_to(url_for('/index.php'));

 ?>
