$(document).ready(function(){
  AddDiscAttribute();
  $("#typeswitcher").on('change', function() {
      if ($("#typeswitcher").val() == "size") {
        AddDiscAttribute();
      } else if ($("#typeswitcher").val() == "weight") {
        AddBookAttribute();
      } else if ($("#typeswitcher").val() == "dimensions") {
        AddFurnituteAttribute();
      }
  });
});

function AddDiscAttribute () {
  $("#attribute").html('<label for="size">Size</label><input class="addinput" type="number" name="size" placeholder="Type product&#39s size here" required></br>');
}

function AddBookAttribute () {
  $("#attribute").html('<label for="weight">Weight</label><input class="addinput" type="number" name="weight" placeholder="Type product&#39s weight here" required></br>');
}

function AddFurnituteAttribute () {
  $("#attribute").html('<label for="dimensionsH">Height</label><input class="addinput" type="number" name="dimensionsH" placeholder="Type product&#39s height here" required></br>' +
  '<label for="dimensionsW">Width</label><input class="addinput" type="number" name="dimensionsW" placeholder="Type product&#39s width here" required></br>' +
  '<label for="dimensionsL">Length</label><input class="addinput" type="number" name="dimensionsL" placeholder="Type product&#39s length here" required></br>');
}
