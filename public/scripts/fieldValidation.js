var abort = false;

function isValid () {
  if (abort) { return false; } else { return true; }
}

function validation () {
  var str = $('#skuinput').val();
  $("div.error").remove();

  if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
  } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if (this.responseText == "skuExists") {
            abort = true;
            $('#skuinput').after('<div class="error">SKU already exists. Please type another one.</div>');
          } else {
            abort = false;
          }
      }
  };
  xmlhttp.open("GET","skuvalidation.php?q="+str,true);
  xmlhttp.send();
}
