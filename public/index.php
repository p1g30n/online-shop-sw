<?php

  require_once('../private/initialize.php');

  $products = create_product_instances();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Online Shop</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo url_for('/stylesheets/styles.css'); ?>" />
  </head>
  <body>

    <h1>Available Products</h1><hr>
    <form method="post" action="<?php echo url_for('/delete.php'); ?>">
      <div id="productholder">
        <?php for ($i=0; $i < count($products); $i++) { ?>
          <div class="product">
            <input type="hidden" name="<?php echo $products[$i]->get_id(); ?>" type="checkbox" value="off">
            <input class="cbox" name="<?php echo $products[$i]->get_id(); ?>" type="checkbox" value="on">
            <div>
              <?php echo $products[$i]->get_sku(); ?><br>
              <?php echo $products[$i]->get_name(); ?><br>
              <?php echo $products[$i]->get_price(); ?> $<br>
              <?php echo $products[$i]->get_attribute(); ?>
            </div>
          </div>
        <?php } ?>
      </div>

      <div id="linkholder">
        <a id="addbtn" href=<?php echo url_for('/add_product.php'); ?> >Add Product</a><br>
        <input id="deletebtn" type="submit" value="Delete Product">
      </div>
    </form>

    <?php
      session_start();
      $_SESSION['products'] = $products;
     ?>
  </body>
</html>
