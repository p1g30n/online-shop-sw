<?php

  require_once('../private/initialize.php');

  $generalArgs = [
    'sku' => h($_POST['sku'] ?? ''),
    'name' => h($_POST['name'] ?? ''),
    'price' => $_POST['price'] ?? '',
    'type' => $_POST['type'] ?? ''
  ];

  switch ($_POST['type']) {
    case 'size':
      $product = new DVDDisc(
        $generalArgs,
        ['size' => $_POST['size'] ?? '']
      );
      break;

    case 'weight':
      $product = new Book(
        $generalArgs,
        ['weight' => $_POST['weight'] ?? '']
      );
      break;

    case 'dimensions':
      $product = new Furniture(
        $generalArgs,
        [
          'dimensionsH' => $_POST['dimensionsH'] ?? '',
          'dimensionsW' => $_POST['dimensionsW'] ?? '',
          'dimensionsL' => $_POST['dimensionsL'] ?? ''
        ]
      );
      break;

    default:
      $product = new Product(
        $generalArgs
      );
      break;
  }

  insert_product_into_db($product);

  redirect_to(url_for('/index.php'));

?>
