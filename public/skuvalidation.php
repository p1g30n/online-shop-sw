<?php

  require_once('../private/initialize.php');

  $q = strval($_GET['q']);
  $result = get_sku_from_db($q);

  if ($result->num_rows > 0) {
    echo "skuExists";
  } else {
    echo "skuDoesntExist";
  }

 ?>
