<?php

 function get_all_products_from_db() {
   global $db;

   $query = "SELECT * FROM products";
   $result = mysqli_query($db, $query);
   return $result;
 }

 function get_sku_from_db ($sku) {
   global $db;

   $query = "SELECT * FROM products WHERE sku='";
   $query .= $sku;
   $query .= "'";
   $result = mysqli_query($db, $query);
   return $result;
 }

 function insert_product_into_db ($product) {
   global $db;

   $query = "INSERT INTO products VALUES (default, ";
   $query .= "'" . $product->get_sku() . "', ";
   $query .= "'" . $product->get_name() . "', ";
   $query .= $product->get_price() . ", ";
   $query .= "'" . $product->get_type() . "', ";
   $query .= "'" . $product->get_attribute() . "'";
   $query .= ")";
   $result = mysqli_query($db, $query);

   if($result) {
      return true;
    } else {
      db_disconnect($db);
      exit;
    }
 }

 function delete_products_from_db ($id) {
   global $db;

   $query = "DELETE FROM products WHERE product_id=";
   $query .= $id;
   $result = mysqli_query($db, $query);

   if($result) {
      return true;
    } else {
      db_disconnect($db);
      exit;
    }
 }

?>
