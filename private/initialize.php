<?php

  define("PRIVATE_PATH", dirname(__FILE__));
  define("PROJECT_PATH", dirname(PRIVATE_PATH));
  define("PUBLIC_PATH", PROJECT_PATH . '/public');
  define("SHARED_PATH", PRIVATE_PATH . '/shared');

  $public_end = strpos($_SERVER['SCRIPT_NAME'], '/public') + 7;
  $doc_root = substr($_SERVER['SCRIPT_NAME'], 0, $public_end);
  define("WWW_ROOT", $doc_root);

  function my_autoload($class) {
      if(preg_match('/\A\w+\Z/', $class)) {
        include('classes/' . $class . '.class.php');
      }
    }
  spl_autoload_register('my_autoload');

  require_once('functions/functions.php');
  require_once('database/db_functions.php');
  require_once('database/query_functions.php');

  $db = db_connect();

?>
