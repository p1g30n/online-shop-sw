<?php

  require_once('../private/initialize.php');

  function url_for($script_path) {
    if($script_path[0] != '/') {
      $script_path = "/" . $script_path;
    }
    return WWW_ROOT . $script_path;
  }

  function redirect_to($location) {
    header("Location: " . $location);
    exit;
  }

  function h($string="") {
    return htmlspecialchars($string);
  }

  function create_product_instances () {
    $result_set = get_all_products_from_db();
    $products = [];
    while ($db_product = mysqli_fetch_assoc($result_set)) {
      $products[] = new Product([
        'id' => $db_product['product_id'],
        'sku' => $db_product['sku'],
        'name' => $db_product['name'],
        'price' => $db_product['price'],
        'type' => $db_product['type'],
        'attribute' => $db_product['attribute']]);
    }
    return $products;
  }

?>
