<?php

  class Book extends Product {

    function __construct($generalArgs=[], $args=[]) {
      parent:: __construct($generalArgs);
      $this->set_attribute($args['weight'] ?? '');
    }

    function set_attribute($weight) {
      $this->attribute = $weight . ' KG';
    }
  }

 ?>
