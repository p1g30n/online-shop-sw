<?php

  class Furniture extends Product {

    function __construct($generalArgs=[], $args=[]) {
      parent:: __construct($generalArgs);
      $this->set_attribute(
        $args['dimensionsH'] ?? '',
        $args['dimensionsW'] ?? '',
        $args['dimensionsL'] ?? ''
      );
    }

    function set_attribute($dimensionsH, $dimensionsW, $dimensionsL) {
      $this->attribute = $dimensionsH . 'x' . $dimensionsW . 'x' . $dimensionsL;
    }
  }

 ?>
