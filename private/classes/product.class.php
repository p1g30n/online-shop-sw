<?php

  class Product {

    var $id;
    var $sku;
    var $name;
    var $price;
    var $type;
    var $attribute;

    function __construct($args=[]) {
      $this->id=$args['id'] ?? '';
      $this->sku=$args['sku'] ?? '';
      $this->name=$args['name'] ?? '';
      $this->price=$args['price'] ?? '';
      $this->type=$args['type'] ?? '';
      $this->attribute=$args['attribute'] ?? '';
    }

    public function get_id () {
      return $this->id;
    }

    public function get_sku() {
      return $this->sku;
    }

    public function get_name() {
      return $this->name;
    }

    public function get_price() {
      return $this->price;
    }

    public function get_type() {
      return $this->type;
    }

    public function get_attribute () {
      return $this->attribute;
    }
  }

?>
