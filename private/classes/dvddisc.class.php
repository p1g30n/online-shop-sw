<?php

  class DVDDisc extends Product {

    function __construct($generalArgs=[], $args=[]) {
      parent:: __construct($generalArgs);
      $this->set_attribute($args['size'] ?? '');
    }

    function set_attribute($size) {
      $this->attribute = $size . ' MB';
    }
  }

 ?>
