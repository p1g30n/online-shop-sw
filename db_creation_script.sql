CREATE SCHEMA IF NOT EXISTS `all_products` DEFAULT CHARACTER SET utf8;
USE `all_products`;

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
    `product_id` INT(10) NOT NULL AUTO_INCREMENT,
    `sku` CHAR(32) NOT NULL,
    `name` CHAR(32) NOT NULL,
    `price` INT(16),
    `type` CHAR(64),
    `attribute` CHAR(64),
    PRIMARY KEY(`product_id`)
);